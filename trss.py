#!/usr/bin/env python

import transmissionrpc
import sys
import argparse


class TRemote:
    trc = None
    def conect(self,address):
        self.trc = transmissionrpc.Client(address)
    def start(self):
        print "starting not finished torrent ..."
        torrent_list = self.trc.get_torrents()
        not_complete_list = [x for x in torrent_list if x.progress < 100]
        for x in not_complete_list: print str(x.name)
        not_complete_ids = [x.id for x in not_complete_list]
        self.trc.start_torrent(not_complete_ids)

    def stop(self):
        print "stop all torrent"
        torrent_list = self.trc.get_torrents()
        ids = [x.id for x in torrent_list]
        for x in torrent_list: print str(x.name)
        self.trc.stop_torrent(ids)


def main():
    tr = TRemote()

    parser = argparse.ArgumentParser()
    parser.add_argument("--remote",
                        default="127.0.0.1",
                        help="Transmission Server",
                        dest="ADDRESS")
    sub = parser.add_subparsers()
    parser_start = sub.add_parser("start")
    parser_start.set_defaults(func=tr.start)

    parser_stop = sub.add_parser("stop")
    parser_stop.set_defaults(func=tr.stop)
    args =  parser.parse_args()
    tr.conect(args.ADDRESS)
    args.func()



if __name__ == '__main__':
    main()
